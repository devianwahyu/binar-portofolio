# BINAR PORTOFOLIO TASK
## Description
Ini adalah salah satu tugas selama mengikuti bootcamp dari Binar Academy Kampus Merdeka. Project ini adalah sebuah website portofolio sederhana.

## Technologies
- HTML5
- CSS3
- Flex

## Design Reference
- github

## Demo
link: https://binar-portofolio.vercel.app/
